class Actor {
public :
    int x,y; // position on map
    int ch; // ascii code
    TCODColor col; // color
	const char *name; // the actor's name
	bool blocks; // can we walk on this actor?
	Ai *ai; // something self-updating
	Pickable *pickable; //something that can be picked up and used
    Container *container; //something that can contain Actors

	Actor(int x, int y, int ch, const char *name, const TCODColor &col);
	~Actor();
	void update();
    void render() const;
};
