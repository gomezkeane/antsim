class Ai {
public :
	virtual void update(Actor *owner)=0;
};


class AntAi : public Ai {
public :
	AntAi();
	void update(Actor *owner);
protected :
	int direction;
	int oldDirection;
	enum Status {
        WANDER,
        RETURN_NEST,
        EXAMINE_SCENT,
        FOLLOW_SCENT,
	} status;
	bool moveAnt(Actor *owner);
	void pickUpFood(Actor *owner);
	void dropFood(Actor *owner);
};

class NestAi : public Ai {
public:
    NestAi();
    void update(Actor *owner);
protected:
    void pickUpFood(Actor *owner);
};

