class Engine {
public :
	enum GameStatus {
		STARTUP,
		IDLE,
		NEW_TURN,
		NEW_ANT,
		VICTORY,
		DEFEAT
	} gameStatus;
	TCOD_key_t lastKey;
    TCODList<Actor *> actors;
    Actor *nest;
    Map *map;

    int screenWidth;
    int screenHeight;


    Engine(int screenWidth, int screenHeight);
    ~Engine();
    void update();
    void render();

};

extern Engine engine;
