struct Tile {
    bool explored; // has the player already seen this tile ?
    unsigned int scent;
    Tile() : explored(false), scent(0) {}
};

class Map {
public :
    int width,height;

    Map(int width, int height);
    ~Map();
    void render() const;
    void requestAnt();


    unsigned int getScent(int x, int y) const;
    void setScent(int x, int y);
    void decayScent(int x,int y);

protected :
    Tile *tiles;
    void addAnt(int x,int y);
    void addFood (int x,int y);
};
