
class Effect {
public :
	virtual bool applyTo(Actor *actor) = 0;
};

class HealthEffect : public Effect {
public :
	float amount;

	HealthEffect(float amount);
	bool applyTo(Actor *actor);
};



class Pickable {
public :
	Pickable(Effect *effect);
	virtual ~Pickable();
	bool pick(Actor *owner, Actor *wearer);
	void drop(Actor *owner, Actor *wearer);
	bool use(Actor *owner, Actor *wearer);
protected :
	Effect *effect;
};
