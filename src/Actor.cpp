#include <math.h>
#include "main.hpp"

Actor::Actor(int x, int y, int ch, const char *name,
    const TCODColor &col) :
    x(x),y(y),ch(ch),col(col),name(name),blocks(true),ai(NULL),
    pickable(NULL),container(NULL)
    {

    }

Actor::~Actor() {
    if ( ai ) delete ai;
    if ( pickable) delete pickable;
    if ( container ) delete container;
}

void Actor::render() const {
    TCODConsole::root->setChar(x,y,ch);
    TCODConsole::root->setCharForeground(x,y,col);
}

void Actor::update() {
if ( ai ) ai->update(this);
}


