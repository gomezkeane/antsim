#include <stdio.h>
#include <math.h>
#include "main.hpp"


AntAi::AntAi() : direction(0), oldDirection(0), status(WANDER) {
}

void AntAi::update(Actor *owner) {
    if (this->status == WANDER){
    if (DEBUG)owner->col = TCODColor::lightRed;
    if (engine.map->getScent(owner->x, owner->y)>0){
      this->status = EXAMINE_SCENT;
      return;
    }
    TCODRandom *rng=TCODRandom::getInstance();
    int dice=rng->getInt(0,100);
    if (dice < 25)this->direction=rng->getInt(1,4);

    if (moveAnt(owner))pickUpFood(owner);
    }
    else if(this->status == RETURN_NEST)
    {
    if (DEBUG)owner->col=TCODColor::lightYellow;
    engine.map->setScent(owner->x, owner->y);

    int dx = 10 - owner->x;
    int dy = 35 - owner->y;
    this->direction = (dx < 0 ? 1:3);
    if (dx) moveAnt(owner);

    if (!dx){
        this->direction = (dy < 0 ? 2:4);
        if (dy) moveAnt(owner);
    }

    if (!dx && !dy) dropFood(owner);

    }
    else if(this->status == EXAMINE_SCENT){
        if (DEBUG)owner->col = TCODColor::black;
        unsigned int bestLevel=0;
        static int tdx[4]={-1,0,1,0};
        static int tdy[4]={0,1,0,-1};

        for (int i=0; i<  4; i++) {
            int cellx=owner->x+tdx[i];
            int celly=owner->y+tdy[i];
                    unsigned int cellScent = engine.map->getScent(cellx,celly);
                if (cellScent >= bestLevel) {
                        bestLevel=cellScent;
                        this->direction=i+1;
                }
         }
         this->status = FOLLOW_SCENT;


        if (engine.map->getScent(owner->x,owner->y) <= 0) this->status = WANDER;
    }
    else if(this->status == FOLLOW_SCENT){

    TCODRandom *rng=TCODRandom::getInstance();
    bool choice = rng->getInt(0,1);

            switch (this->direction){
        case 1:
            if (engine.map->getScent(owner->x,owner->y+1)){
                if(engine.map->getScent(owner->x-1,owner->y)<=0){
                    this->direction = 4;
                }
                else {
                    if (choice) this->direction = 4;
                }


            }
            else if (engine.map->getScent(owner->x,owner->y-1)){
                     if (engine.map->getScent(owner->x-1,owner->y)<=0){
                        this->direction = 2;
                     }
                     else {
                        if (choice) this->direction = 2;
                     }
            }
            break;
        case 2:
            if (engine.map->getScent(owner->x+1,owner->y)){
                    if (engine.map->getScent(owner->x,owner->y-1)<=0){
                        this->direction = 3;
                    }
                    else {
                        if (choice) this->direction = 3;
                    }
            }
            else if (engine.map->getScent(owner->x-1,owner->y)){
                     if (engine.map->getScent(owner->x,owner->y-1)<=0){
                this->direction = 1;
            }
                else {
                    if (choice) this->direction = 1;
                }
            }
            break;
        case 3:
            if (engine.map->getScent(owner->x,owner->y+1)){
                    if (engine.map->getScent(owner->x+1,owner->y)<=0){
                this->direction = 2;
            }
                else {
                    if (choice) this->direction = 2;
                }
            }
            else if (engine.map->getScent(owner->x,owner->y-1)){
                    if (engine.map->getScent(owner->x+1,owner->y)<=0){
                this->direction = 3;
            }
            else {
                if (choice) this->direction = 3;
            }
            }
            break;
        case 4:
            if (engine.map->getScent(owner->x+1,owner->y)){
                    if (engine.map->getScent(owner->x,owner->y+1)<=0){
                this->direction = 3;
            }
            else {
                if (choice) this->direction = 3;
            }
            }
            else if (engine.map->getScent(owner->x-1,owner->y)){
                    if (engine.map->getScent(owner->x,owner->y+1)<=0){
                this->direction = 1;
            }
            else {
                if (choice) this->direction = 1;
            }
            }
            break;
            }

            if (moveAnt(owner))pickUpFood(owner);
            if (engine.map->getScent(owner->x,owner->y) <= 0) this->status = WANDER;
    }

}


bool AntAi::moveAnt(Actor *owner){
    switch (this->direction) {

    case 1:
    if (owner->x>0){
            owner->x--;
            return true;
    }
    break;

    case 2:
    if (owner->y>0){
            owner->y--;
            return true;
    }
    break;

    case 3:
    if (owner->x<79){
            owner->x++;
            return true;
    }
    break;

    case 4:
    if (owner->y<49){
            owner->y++;
            return true;
    }
    break;

    default:{

    }
    }
    return false;
}

void AntAi::pickUpFood(Actor *owner){
    bool found=false;
    static int tdx[4]={-1,0,1,0};
    static int tdy[4]={0,1,0,-1};
    for (int i=0; i<  4; i++) {
            //if (found)break;
        for (Actor **iterator=engine.actors.begin();
				iterator != engine.actors.end(); iterator++) {
				Actor *actor=*iterator;
				if ( actor->pickable && actor->x == owner->x+tdx[i] && actor->y == owner->y+tdy[i] ) {
					if (actor->pickable->pick(actor,owner)) {
						found=true;
						this->status = RETURN_NEST;
						engine.gameStatus=Engine::NEW_ANT;
						//pickup
						break;
					} else if (! found) {
						found=true;
						//inventory full
					}
				}
			}
        }
}

void AntAi::dropFood(Actor *owner){
    this->status = WANDER;
    owner->pickable->drop(owner->container->inventory.get(0) , owner);


}

NestAi::NestAi(){}

void NestAi::update(Actor *owner){
    engine.gameStatus=Engine::NEW_TURN;
    pickUpFood(owner);

}

void NestAi::pickUpFood(Actor *owner){
    bool found=false;
    for (Actor **iterator=engine.actors.begin();
				iterator != engine.actors.end(); iterator++) {
				Actor *actor=*iterator;
				if ( actor->pickable && actor->x == owner->x && actor->y == owner->y ) {
					if (actor->pickable->pick(actor,owner)) {
						found=true;

                        engine.map->requestAnt();
						//pickup
						break;
					} else if (! found) {
						found=true;
						//inventory full
					}
				}
			}
}
