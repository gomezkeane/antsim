
#include "main.hpp"

Engine::Engine(int screenWidth, int screenHeight) : screenWidth(screenWidth),screenHeight(screenHeight) {
    TCODConsole::initRoot(screenWidth,screenHeight,"Antsim",false);
    TCODSystem::setFps(25);

    map = new Map(80,50);

    nest = new Actor(10,35,'>',"Nest",TCODColor::black);
    nest->ai = new NestAi();
    nest->container = new Container(256);
    actors.push(nest);

}

Engine::~Engine() {
    actors.clearAndDelete();
    delete map;
}

void Engine::update() {
   	gameStatus=IDLE;
    TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS,&lastKey,NULL);
    if ( lastKey.vk == TCODK_ENTER && lastKey.lalt ) {
    	TCODConsole::setFullscreen(!TCODConsole::isFullscreen());
    }
    nest->update();

    if ( gameStatus == NEW_TURN ) {

        for (int x=0; x < 80; x++) {
            for (int y=0; y < 50; y++) {
                map->decayScent(x,y);

	        }
   	    }

	    for (Actor **iterator=actors.begin(); iterator != actors.end();
	        iterator++) {
	        Actor *actor=*iterator;
	        if (actor != nest){
	            actor->update();
            }

            if ( gameStatus != NEW_TURN) break; // Ugly Hack to stop irterator becoming invalidated and causing a segfault
	    }

	}
	if (gameStatus == NEW_ANT){

        //map->requestAnt();

        //gameStatus = NEW_TURN;
	}
}

void Engine::render() {
	TCODConsole::root->clear();
	map->render();
	// draw the actors
	for (Actor **iterator=actors.begin();
	    iterator != actors.end(); iterator++) {
		Actor *actor=*iterator;
		actor->render();

	}
}
