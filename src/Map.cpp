#include "main.hpp"


static const int MAX_ANTS = 1;
static const int MAX_FOOD = 10;
static const int MAX_NEST = 1;
static const int GRID_WIDTH = 5;
static const int GRID_HEIGHT = 5;
static const int SCENT_ITENSITY = 200;
static const int SCENT_DECAY = 1;

Map::Map(int width, int height) : width(width),height(height){
    tiles=new Tile[width*height];

    TCODRandom *rng = TCODRandom::getInstance();

    //add ants
    int nbAnts = rng->getInt(1,MAX_ANTS);
    while (nbAnts > 0){
        addAnt(10, height - 10);
        nbAnts--;
    }

    //add food
    int nbFood = rng->getInt(1,MAX_FOOD);
    while (nbFood > 0){
        int x = rng->getInt(0,width - GRID_WIDTH);
        int y = rng->getInt(0,height - GRID_HEIGHT);
        addFood(x,y);
        nbFood--;
    }

}

Map::~Map() {
    delete [] tiles;
}

void Map::render() const {

	for (int x=0; x < width; x++) {
	    for (int y=0; y < height; y++) {
            TCODConsole::root->setCharBackground(x,y,TCODColor::sepia );
            if(DEBUG && this->getScent(x,y) > 0)TCODConsole::root->setCharBackground(x,y,TCODColor::darkerSepia);

	        }
   	    }
	}

void Map::addAnt(int x,int y){
    Actor *ant = new Actor(x,y,'a',"ant",TCODColor::black);
    ant->ai = new AntAi();
    ant->container = new Container(1);
    engine.actors.push(ant);
}

void Map::addFood(int x,int y){
    /////////////////////////////////////////////Actor *food = new Actor(x,y,'%',"Food",TCODColor::lightGreen);
    /////////////////////////////////////////////food->pickable = new Pickable(NULL);
    /////////////////////////////////////////////engine.actors.push(food);
    TCODRandom *rng = TCODRandom::getInstance();
    bool foodPresent;
    for (int cellx=0; cellx < GRID_WIDTH; cellx++){
        for (int celly=0; celly < GRID_HEIGHT; celly++){

            foodPresent = rng->getInt(0,1);
            if (foodPresent){
                Actor *food = new Actor( x + cellx, y + celly, '%',"Food",TCODColor::lightGreen);
                food->pickable = new Pickable(NULL);
                engine.actors.push(food);
            }
        }
    }
}



void Map::requestAnt(){
    addAnt(10,height - 10);
}


unsigned int Map::getScent(int x, int y)const {
    return tiles[x + y * width].scent;
}

void Map::setScent(int x, int y){
    tiles[x + y * width].scent += SCENT_ITENSITY;
}

void Map::decayScent(int x, int y){
    TCODRandom *rng = TCODRandom::getInstance();
    bool noise =rng->getInt(0,SCENT_DECAY);
    if (tiles[x + y * width].scent >0 && noise) tiles[x + y * width].scent -= SCENT_DECAY;
}
