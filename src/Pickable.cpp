#include "main.hpp"


Pickable::Pickable(Effect *effect) :
	effect(effect) {
}

Pickable::~Pickable() {
	if ( effect ) delete effect;
}

bool Pickable::pick(Actor *owner, Actor *wearer) {
	if ( wearer->container && wearer->container->add(owner) ) {
		engine.actors.remove(owner);
		return true;
	}
	return false;
}

void Pickable::drop(Actor *owner, Actor *wearer) {
	if ( wearer->container ) {
		wearer->container->remove(owner);
		engine.actors.push(owner);
		owner->x=wearer->x;
		owner->y=wearer->y;

	}
}

